if [ $# -eq 0 ]
then
	echo "$0 : Please provide the master host"
	exit 1
fi

echo "Starting worker on $1..."

java -Xmx5000m -cp . -Djava.rmi.server.codebase=http://$1:18114/KaichuBenchmark.jar -Djava.security.policy=worker.policy -jar Kaichu.jar --rmiMaster $1
