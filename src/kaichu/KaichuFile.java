/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class KaichuFile implements Serializable {
    
    
    private String filename;
    private int lengthOfLast;
    private ArrayList<byte[]> data = new ArrayList<byte[]>();
    
    public KaichuFile(File file) throws FileNotFoundException, IOException {
        filename = file.getName();
        
        InputStream is = new FileInputStream(file);
        DataInputStream din = new DataInputStream(is);
        
        int read = -1;
        
        do {
            lengthOfLast = read;
            byte[] buffer = new byte[1024];
            read = din.read(buffer);
            //System.out.println(read);
            if(read != -1) {
                data.add(buffer);
            }
            
        } while(read != -1);

    }
    
    public void save() throws FileNotFoundException, IOException {
        
        File file = new File(filename);
        
        if(file.exists()) {
            return;
        }
        
        OutputStream os = new FileOutputStream(file);
        DataOutputStream dout = new DataOutputStream(os);
        
        for (int i = 0; i < data.size() - 1; i++) {
            dout.write(data.get(i));
        }

        // last bit
        dout.write(data.get(data.size()-1), 0, lengthOfLast);
        
        dout.close();
        os.close();
    }
}
