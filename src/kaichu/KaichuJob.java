/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu;

import java.io.Serializable;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class KaichuJob implements Serializable {

    private KaichuParameters parameters;
    private Class taskClass;

    public KaichuJob(KaichuParameters parameters, Class taskClass) {
        
        if(parameters == null) 
            throw new RuntimeException("Parameters cannot be null in a KaichuJob!");
        
        if(taskClass == null) 
            throw new RuntimeException("The task class cannot be null in a KaichuJob!");    
	
	if(!KaichuTask.class.isAssignableFrom(taskClass))
	    throw new RuntimeException("The task object has to be an instance of SanchoTask subclass.");
	
        this.parameters = parameters;
        this.taskClass = taskClass;
    }

    public KaichuParameters getParameters() {
	return parameters;
    }
    
    public Class getTaskClass() {
	return taskClass;
    }
}
