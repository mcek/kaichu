/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu;

import java.io.Serializable;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class KaichuResponseCapsule implements Serializable {
    long ID;
    long roundID;
    public KaichuResponse response;

    public KaichuResponseCapsule(KaichuResponse response, KaichuJobCapsule capsule) {
	this.ID = capsule.getID();
	this.roundID = capsule.getRoundID();
	this.response = response;
    }
    
    public long getID() {
	return ID;
    }
    
    public long getRoundID() {
	return roundID;
    }
}
