/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class KaichuUtility {
    public static int sizeOfSerialized(Serializable ser) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(ser);
            oos.close();
        } catch(IOException ex) {
            Logger.getLogger(KaichuUtility.class.getName()).log(Level.SEVERE, null, ex);
        }

        return baos.size();
    }

    public static void printMessageSizes() {
    /*    TaskParameters tp = new TaskParameters();
        tp.databaseSize = 380760;
        tp.mirna = "GAACCGAGTTGATCTCCTATG";
        byte[] lala = {-2, -6, 0, -4, -5, -2, -1, 1, -1, -2, -3, -5};
        tp.swParameters = new byte[12];
        System.arraycopy(lala, 0, tp.swParameters, 0, 12);
        System.out.println("Size of Job: " + sanchocluster.KaichuUtility.sizeOfSerialized(new SanchoJob("Tralala", tp, 1206, 192837)));

        System.out.println("Size of Response: " + sanchocluster.KaichuUtility.sizeOfSerialized(new SanchoResponse(1206, 45679, 1934873)));*/
    }



    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]");

    public static String getDate() {
        return dateFormatter.format(new Date());
    }

    public static int getCondorID() {
        File file = new File(".");

        String[] strings = file.list();

        ArrayList<String> outfiles = new ArrayList<String>();

        for(String s : strings) {
            if(Pattern.matches("^out\\.[0-9]+", s))
                outfiles.add(s);
        }

        if(outfiles.size() > 1) {
            System.out.println("I cannot determine the condor ID because there are more than one files matching the out file:");
            for(String s : outfiles)
                System.out.println(Integer.parseInt(s.substring(4)));
            System.out.println("It's fine if you are not using Condor.");
        } else if(outfiles.isEmpty()) {
            System.out.println("I cannot determine the condor ID because there is no file identified as the out file which is fine if you are not using Condor.");
        } else
            return Integer.parseInt(outfiles.get(0).substring(4));

        return -1;

    }

    static StringBuilder sb = new StringBuilder();
    static Formatter formatter = new Formatter(sb, Locale.UK);

    public static String formatNumber(long number) {
	sb.delete(0, sb.length());

	//String formatted;
	if(number > 1000000000) {
	    //formatted = Long.toString(number / 1073741824) + " GB";
	    formatter.format("%.1f GiB", number / 1073741824.0);
	} else if(number > 1000000) {
	    //formatted = Long.toString(number / 1048576) + " MiB";
	    formatter.format("%.1f MiB", number / 1048576.0);
	} else if(number > 1000) {
	    //formatted = Long.toString(number / 1024) + " KiB";
	    formatter.format("%.1f KiB", number / 1024.0);
	} else {
	    //formatted = Long.toString(number) + " bytes";
	    sb.append(number);
	    sb.append(" bytes");
	}

	return sb.toString();
    }
}
