/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import kaichu.monitor.KaichuState;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public interface MasterStub extends KaichuProcess, Remote {

    public KaichuTask getTask(Class taskClass, int workerID) throws RemoteException;
    public Object getResource(String id, int workerID) throws RemoteException;
    public void gotTask(int workerID) throws RemoteException;
    public void gotResource(String id, int workerID) throws RemoteException;
    public int hello(ExecutorStub stub, String host, int condorID) throws RemoteException;
    public boolean areWeStillOnWithThis(int threadID) throws RemoteException;
    public ResourceBrokerStub getR(String id, int workerID) throws RemoteException;
    public void iPossessResource(String id, ResourceBrokerStub worker) throws RemoteException;
    
    public void returnResponse(List<KaichuResponseCapsule> resposes, int workerID) throws RemoteException;
    
    public KaichuState getState() throws RemoteException;    
    public void terminateWorker(int workerID) throws RemoteException;
}
