/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.master.KaichuManager;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class ResourceSender implements RobustMessengerDelegate {

        ResourceBrokerStub recipient;
        String id;
        Object resource;

        public ResourceSender(ResourceBrokerStub recipient, String id, Object resource) {
            this.recipient = recipient;
            this.id = id;
            this.resource = resource;
        }

    @Override
        public boolean execute() {
	try {
	    
	    recipient.sendResource(id, resource);
	} catch(RemoteException ex) {
	    if(KaichuManager.getDebug())
		System.out.println("ResourceSender.execute(): failed, RemoteException");
		Logger.getLogger(ResourceSender.class.getName()).log(Level.SEVERE, null, ex);
	    return false;
	}

            return true;
        }
    }