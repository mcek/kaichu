/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu;

import java.rmi.RemoteException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import kaichu.master.KaichuManager;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class RobustMessenger {
    
    
    ExecutorService executorService;
    
    static int INTERROGATION_PERIOD = 60;  // in seconds
    static int INTERROGATION_TIMEOUT = 5;  // in seconds
    
    boolean success;
    
    public RobustMessenger(ExecutorService executorService) {
        this.executorService = executorService;
    }
    
    public boolean perform(KaichuProcess receiver, RobustMessengerDelegate delegate) {
        Future message = executorService.submit(new Performer(delegate));
	
        success = true;
        while (true) {
            try {
                message.get(INTERROGATION_PERIOD, TimeUnit.SECONDS);
                break; // if we are here it means that the future finished
            } catch (InterruptedException ex) {
		if(KaichuManager.getDebug()) System.out.println("InterruptedException in RobustMessenger.perform() [1]!");
                return false;
            } catch (ExecutionException ex) {
		if(KaichuManager.getDebug()) System.out.println("ExecutionException in RobustMessenger.perform() [1]!");
                return false;
            } catch (TimeoutException ex) {
		if(KaichuManager.getDebug()) System.out.println("First TimeoutException in RobustManager.perform(), trying again...");
                // time to check whether the worker is still live
                Future interrogator = executorService.submit(new Interrogator(receiver));
                try {
                    //System.out.println("Checking if live...");
                    interrogator.get(INTERROGATION_TIMEOUT, TimeUnit.SECONDS);
                } catch (InterruptedException ex1) {
		    if(KaichuManager.getDebug()) System.out.println("InterruptedException in RobustMessenger.perform() [2]!");
                    return false;
                } catch (ExecutionException ex1) {
		    if(KaichuManager.getDebug()) System.out.println("ExecutionException in RobustMessenger.perform() [2]!");
                    return false;
                } catch (TimeoutException ex1) {  //  timed out, but let's try again
		    if(KaichuManager.getDebug()) System.out.println("Second TimeoutException in RobustManager.perform(), trying again...");
                    interrogator.cancel(true);
                    interrogator = executorService.submit(new Interrogator(receiver));
                    try {
                        interrogator.get(INTERROGATION_TIMEOUT, TimeUnit.SECONDS);
                    } catch (InterruptedException ex2) {
			if(KaichuManager.getDebug()) System.out.println("InterruptedException in RobustMessenger.perform() [3]!");
                        return false;
                    } catch (ExecutionException ex2) {
			if(KaichuManager.getDebug()) System.out.println("ExecutionException in RobustMessenger.perform() [3]!");
                        return false;
                    } catch (TimeoutException ex2) {
			if(KaichuManager.getDebug()) System.out.println("TimeoutException in RobustMessenger.perform()!");
                        return false;
                    }
                }
            }
        }

        return success;
    }

    private class Performer implements Runnable {
        
        RobustMessengerDelegate delegate;
        
        Performer(RobustMessengerDelegate delegate) {
            this.delegate = delegate;
        }

	@Override
        public void run() {
            success = delegate.execute();
        }
    }

    private class Interrogator implements Runnable {

        KaichuProcess receiver;

        
        Interrogator(KaichuProcess receiver) {
            this.receiver = receiver;
        }

	@Override
	public void run() {
	    success = true;
	    try {
		success = receiver.isLive();
	    } catch(RemoteException ex) {
		success = false;
	    }
	}
    }
}
