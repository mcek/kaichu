/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.util.LinkedList;
import java.util.List;
import kaichu.KaichuJob;
import kaichu.KaichuJobCapsule;
import kaichu.KaichuResponseCapsule;
import kaichu.KaichuUtility;
import kaichu.monitor.StateHolder;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
class JobManager {

    //private TreeMap<Integer, SanchoResponse> responses = new TreeMap<Integer, SanchoResponse>();

    private LinkedList<KaichuJobCapsule> refusedJobs = new LinkedList<KaichuJobCapsule>();

    KaichuJobCreator jobCreator;

    private KaichuManager sanchoManager;

    //private LinkedBlockingQueue<SanchoResponse> responsess = new LinkedBlockingQueue<SanchoResponse>();

    volatile private long roundID = 0;

    volatile private boolean enabled = false;
    
    private int jobsGenerated = 0;
    
    private int responsesPending = 0; // this shows how many jobs have been submitted minus responses returned

    JobManager(KaichuManager sanchoManager) {
	this.sanchoManager = sanchoManager;
	this.jobCreator = new DispatchQueue();
	//this.jobCreator.manager = sanchoManager;
    }
    
    void setJobCreator(KaichuJobCreator jobCreator) {
	refusedJobs.clear();
	this.jobCreator = jobCreator;
    }

    KaichuJobCapsule getNextJob() {
        if(!enabled)
            return null;

        KaichuJobCapsule capsule;

        synchronized(this) {
            if(!refusedJobs.isEmpty()) {
                capsule = refusedJobs.remove();
                if(KaichuManager.getDebug()) 
		    System.out.println(KaichuUtility.getDate()
                        + " Previously refused job " + capsule.getID() + " assigned.");
                return capsule;
            }
        }

	KaichuJob job = jobCreator.createNewJob();
        
        if(job == null)
            return null;
        
	capsule = new KaichuJobCapsule(sanchoManager.nextJobID(), roundID, sanchoManager.getRunID(), job);
        
        synchronized (this) {
            StateHolder.setJobsGenerated(++jobsGenerated);
            responsesPending++;
        }

        return capsule;
    }
    
    final RunningMean meanCompletionTime = new RunningMean(20, 20);
    long lastChangeTime = 0;
    long minTime = 2000;
    long maxTime = 6000;
    long targetTime = (minTime + maxTime) / 2;
    

    
    private void setNewAggregationSize(Worker worker) {
	if(!sanchoManager.doesAutomaticAggregation())
	    return;
        
        long executionTime = System.currentTimeMillis() - worker.getExecutionStartTime();
        
        if(lastChangeTime > worker.getExecutionStartTime()) // early ignore the jobs that had been started before the last change
            return;
        
        synchronized(meanCompletionTime) {   
            if(lastChangeTime > worker.getExecutionStartTime()) // ignore the jobs that had been started before the last change
                return;
	    
	    Double average = meanCompletionTime.putValue(executionTime);

	    if(average != null) {
		// set it to between 1 and 5 seconds
		if(average < minTime || average > maxTime) {
		    int newAggregateSize = (int)(sanchoManager.getAggregateSize() * targetTime / average + 0.5);
		                        
                    if(newAggregateSize < 1)   // at least one job in aggregate
                        newAggregateSize = 1;    
                    
		    if(newAggregateSize != sanchoManager.getAggregateSize()) {
                        
                        if(newAggregateSize / sanchoManager.getAggregateSize() > 5) // don't change by more than a factor of 5
                            newAggregateSize = sanchoManager.getAggregateSize() * 5;
                        
                        if(sanchoManager.getAggregateSize() / newAggregateSize > 5)
                            newAggregateSize = sanchoManager.getAggregateSize() / 5;
                        
			System.out.println(KaichuUtility.getDate() + " Mean time is " + average 
				+ " ms. Changing the aggregation from " + sanchoManager.getAggregateSize() + " to " + newAggregateSize);
			sanchoManager.setAggregateSize(newAggregateSize);
			lastChangeTime = System.currentTimeMillis();
			meanCompletionTime.clear();
		    }
		} else {
                    //System.out.println("Haven't changed, the average time is " + average);
                }
	    }
	}
    }
    
    void returnResponses(List<KaichuResponseCapsule> responses, Worker worker) {
	setNewAggregationSize(worker);

	for(KaichuResponseCapsule response : responses) {
	    this.returnResponse(response, worker);
	}

	worker.jobs.clear();
    }

    synchronized private void returnResponse(KaichuResponseCapsule response, Worker worker) {
	if(KaichuManager.getDebug())
	    System.out.println("JobManager: Worker " + worker.id + " returning response " + response.getID());
        if(response.getRoundID() == roundID && sanchoManager.offerResponse(response)) {
	    // hurray!, response offer successful
	    responsesPending--;
	    StateHolder.incrementJobsDoneByWorker(worker.getID());
	} else {
            System.out.println(KaichuUtility.getDate() + " Spurious job "
                    + response.getID() + ":" + response.getRoundID()
                    + " returned from worker "
		    + worker.getID() 
		    + " (" + worker.host + ")");
	    if(response.getRoundID() != roundID) {
		System.out.println(KaichuUtility.getDate() + " : job from a different round");
	    }
	}
    }

    synchronized void iCannotDoTheJob(KaichuJobCapsule jobCapsule, Worker worker) {
        //pendingJobs.remove(job.ID);
        refusedJobs.add(jobCapsule);
        if(KaichuManager.getDebug()) 
	    System.out.println(KaichuUtility.getDate() + " Worker " + worker.id + " (" + worker.host + ") " + " refused job "  + jobCapsule.getID());
    }

    void enable() {
        enabled = true;
    }
    
    void disable() {
	enabled = false;
    }

    void nextRound() {
        roundID++;
    }

    long getRound() {
        return roundID;
    }

    synchronized void clearJobs() {
	responsesPending -= refusedJobs.size();
	refusedJobs.clear();
    }

    int getPendingResponses() {
	return responsesPending;
    }
}
