/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuJobCapsule;
import org.neilja.net.interruptiblermi.InterruptibleRMIThreadFactory;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class JobStarter extends Thread {
    
    final long TIMEOUT = 2 * 1000;
    
    JobManager jobManager;
    WorkerPool workerPool;
    KaichuManager manager;
    
    private volatile boolean die = false;
    
    JobStarter(JobManager jobManager, WorkerPool workerPool, KaichuManager manager) {
	this.jobManager = jobManager;
	this.workerPool = workerPool;
	this.manager = manager;
    }
    
    private boolean success;
    private Worker worker;
    
    ExecutorService executorService = Executors.newCachedThreadPool(InterruptibleRMIThreadFactory.getInstance());
    
    @Override
    public void run() {
	while(!die) {

	    // grab an idle worker

            if (KaichuManager.getDebug()) {
                System.out.println("Taking the next worker...");
            }
            try {
                //while(true) {
                worker = workerPool.idleWorkers.take();
            } catch (InterruptedException ex) {
                if (KaichuManager.getDebug()) {
                    System.out.println("JobStarter exiting.");
                }
                return;
            }

	/*	if(worker != null) 
		    break;
		    
		try {
		    Thread.sleep(100);
		} catch(InterruptedException ex) {
		    //Logger.getLogger(JobStarter.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		if(die) {
		    if(KaichuManager.getDebug()) {
			System.out.println("JobStarter exiting.");
		    }
		    return;
		}
	    }*/
	    
	    if(KaichuManager.getDebug()) 
		System.out.println("Got the next worker! " + worker.getID());


	    if(worker.state == Worker.State.DEAD)
		continue;

	    if(KaichuManager.getDebug()) System.out.println("Before the synchronized block with worker " + worker.getID());
	    synchronized(worker) {
		if(KaichuManager.getDebug()) System.out.println("Inside the synchronized block with worker " + worker.getID());
		// grab job(s)
		do {

		    for(int i = 0; i < manager.getAggregateSize(); i++) {
			KaichuJobCapsule job = jobManager.getNextJob();
			if(job == null) {
			    break;
			}
			worker.jobs.add(job);
		    }

		    if(worker.jobs.isEmpty()) {   // wait a bit if there are no jobs available 
			try {
			    worker.wait(100);
			} catch(InterruptedException ex) {
                            if(KaichuManager.getDebug())
                                Logger.getLogger(JobStarter.class.getName()).log(Level.SEVERE, null, ex);
			}
			if(worker.state == Worker.State.DEAD) {
			    break;
			}
		    }

		    if(die) {
			if(KaichuManager.getDebug())
			    System.out.println("JobStarter exiting.");
			return;
		    }

		} while(worker.jobs.isEmpty());
	    }

		// dispatch! 
		if(KaichuManager.getDebug()) System.out.println("Sending jobs to worker " + worker.getID());
		worker.setState(Worker.State.DL_JOB);
		
	    Thread thread = InterruptibleRMIThreadFactory.getInstance().newThread(new Messenger());
	    done = false;
	    thread.start();

	    do {

		synchronized(thread) {
		    try {
			thread.wait(TIMEOUT);
		    } catch(InterruptedException ex) {
			Logger.getLogger(JobStarter.class.getName()).log(Level.SEVERE, null, ex);
		    }
		}

		if(worker.state == Worker.State.DEAD) {
		    thread.interrupt();
		    success = false;
		    break;
		}
		
	    } while(!done);

	    
	    
		/*Future messenger = executorService.submit(new Messenger());
		boolean timeout = true;
		while(timeout) {
		    timeout = false;
		    try {
			messenger.get(TIMEOUT, TimeUnit.MILLISECONDS);
		    } catch(InterruptedException ex) {
			//Logger.getLogger(JobStarter.class.getName()).log(Level.SEVERE, null, ex);
			success = false;
		    } catch(ExecutionException ex) {
			//Logger.getLogger(JobStarter.class.getName()).log(Level.SEVERE, null, ex);
			success = false;
		    } catch(TimeoutException ex) {
			//Logger.getLogger(JobStarter.class.getName()).log(Level.SEVERE, null, ex);
			timeout = true;
		    }
		    if(die)
			return;
		    if(worker.state == Worker.State.DEAD) {
			success = false;
			break;
		    }
		    
		}*/
		
		if(success) {
		    worker.setState(Worker.State.WORKING);
		    if(KaichuManager.getDebug())
			System.out.println("Worker " + worker.getID() + " accepted jobs from " + worker.jobs.get(0).getID());
		} else {
		    workerPool.dead(worker);
		}
	    
	}
	
	if(KaichuManager.getDebug())
	    System.out.println("JobStarter exiting.");
    }
    
    boolean done;
    
    private class Messenger implements Runnable {

	@Override
	public void run() {
	    try {
		success = worker.execute();
	    } catch(RemoteException ex) {
		if(KaichuManager.getDebug()) Logger.getLogger(JobStarter.class.getName()).log(Level.SEVERE, null, ex);
		success = false;
	    }

	    done = true;
	    
	    synchronized(this) {
		this.notify();
	    }
	}
	
    }
    
    void shutdown() {
	die = true;
	executorService.shutdownNow();
        this.interrupt();
    }
}
