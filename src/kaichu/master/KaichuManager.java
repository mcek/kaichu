/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.io.IOException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuResponse;
import kaichu.KaichuResponseCapsule;
import kaichu.KaichuUtility;
import kaichu.MasterStub;

/**
 *
 * @author Maciej Trybilo, Brunel University
 *
 */

final public class KaichuManager {
    
    private static boolean debug = false;

    private JobManager jobManager;
    private WorkerPool workerPool;
    private Master master;
    private ArrayList<JobStarter> jobStarters = new ArrayList<JobStarter>();
    private int jobStartersNum = 100;
    
    // TODO A very dirty hack here, fix!!!!!!
    public static double cutoff;

    private long runID;

    private long nextResponseID;
    
    private long nextJobID;

    private Registry registry;
    
    private Process httpServer;
    
    private int aggregateSize = 1;
    private boolean automaticAggregationSizing = true;

    final private PriorityBlockingQueue<KaichuResponseCapsule> responsesSorted =
	    new PriorityBlockingQueue<KaichuResponseCapsule>(10, new SanchoResponseCapsuleComparator());

    private final TreeSet<Long> responseIDs = new TreeSet<Long>();
    
    public KaichuManager() {
	initialise();
    }
    
    public KaichuManager(int classServerPort) {
	initialise(classServerPort);
    }
    
    public KaichuManager(int classServerPort, int jobStartersNum) {
        this.jobStartersNum = jobStartersNum;
        initialise(classServerPort);
    }
    
    private void initialise() {
	initialise(18114);
    }

    private void initialise(int classServerPort) {
        
	this.jobManager = new JobManager(this);
	master = new Master();
        ResourceRegistry.master = master;
	workerPool = new WorkerPool(this, jobManager);
	master.workerPool = workerPool;
	
	try {
	    //NanoHTTPD.startServer(classServerPort, false);
	    // In a separate process:
	    //httpServer = Runtime.getRuntime().exec("java -jar lib/NanoHTTPD.jar " + classServerPort + " " + false);
	    ProcessBuilder pb = new ProcessBuilder("java", "-jar", "lib/NanoHTTPD.jar", "" + classServerPort, "false");
	    pb.redirectErrorStream(true);
	   
	    httpServer = pb.start();
	} catch(IOException ex) {
	    Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	/*Thread thread = new Thread(new Runnable() {

	    @Override
	    public void run() {
		BufferedReader br = new BufferedReader(new InputStreamReader(httpServer.getInputStream()));
		String line;
		try {
		    while((line = br.readLine()) != null) {
			System.out.println("NanoHTTPD: " + line);
		    }
		} catch(IOException ex) {
		    Logger.getLogger(SanchoManager.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	    
	});
	
	thread.start();*/
	
	// TODO: write out output from NanoHTTPD
	
	setUpMasterRMI();
	System.out.println("All started");
	nextResponseID = 0;
        nextJobID = 0;
	runID = System.currentTimeMillis();
	setJobCreatorToDispatchQueue();
	
	this.jobStarters = new ArrayList<JobStarter>();
	for(int i = 0; i < this.jobStartersNum; i++) {
	    JobStarter starterThread = new JobStarter(jobManager, workerPool, this);
	    starterThread.start();
	    this.jobStarters.add(starterThread);
	}
    }
    
    public DispatchQueue getDispatchQueue() {
	KaichuJobCreator jcreator = this.jobManager.jobCreator;
	if(jcreator instanceof DispatchQueue)
	    return (DispatchQueue)jcreator;
	else
	    return null;
    }
    
    synchronized long nextJobID() {
        return this.nextJobID++;
    }
    
    public void setJobCreator(KaichuJobCreator jobCreator) {
	//jobCreator.manager = this;
	this.jobManager.setJobCreator(jobCreator);
    }
    
    public void cancelJobs() {
	if(jobManager.jobCreator instanceof DispatchQueue) {
	    ((DispatchQueue)jobManager.jobCreator).clearJobs();
	}
	jobManager.clearJobs();

	while(true) {
	    int p = jobManager.getPendingResponses();
	    System.out.println(KaichuUtility.getDate() + " Pending responses: " + p);
	    if(p == 0)
		break;
	    try {
		// wait for everything to return
		Thread.sleep(2000);
	    } catch(InterruptedException ex) {
		Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}

	// clear responses
	responseIDs.clear();
	responsesSorted.clear();
    }
    
    public DispatchQueue setJobCreatorToDispatchQueue() {
	DispatchQueue queue = new DispatchQueue();
	
	//queue.manager = this;
	this.jobManager.setJobCreator(queue);
	
	return queue;
    }
    
    static public void setDebug(boolean debug) {
        System.out.println("Debug set to " + debug);
	KaichuManager.debug = debug;
    }
    
    static public boolean getDebug() {
	return KaichuManager.debug;
    }

    public long getRunID() {
	return runID;
    }

    /*public void setJobTimeout(int timeout) {
	threadPool.timeout = timeout;
    }

    public int getJobTimeout() {
	return threadPool.timeout;
    }*/

    public KaichuResponseCapsule getNextResponseCapsule() throws InterruptedException {
	return responsesSorted.take();
    }
    
    public KaichuResponseCapsule getNextResponseCapsuleNonBlocking() {
	return responsesSorted.poll();
    }

    // The assumption is that the job IDs start at 0 and are incremented by one
    public KaichuResponseCapsule getNextResponseCapsuleOrdered() throws InterruptedException {

	while(true) {

	    synchronized(responsesSorted) {

		if(!responsesSorted.isEmpty() && responsesSorted.peek().getID() == nextResponseID) {
		    nextResponseID++;
		    return responsesSorted.poll();
		} 

		// we're here, so no nextID thingy
		responsesSorted.wait();
	    }
	}
    }

    public KaichuResponseCapsule getNextResponseCapsuleOrderedNonBlocking() {
	synchronized(responsesSorted) {
	    if(!responsesSorted.isEmpty()) {
		if(responsesSorted.peek().getID() == nextResponseID) {
		    nextResponseID++;
		    return responsesSorted.poll();
		}
	    }
	}
	return null;
    }
    
    public KaichuResponse getNextResponse() throws InterruptedException {
	return this.getNextResponseCapsule().response;
    }
    
    public KaichuResponse getNextResponseNonBlocking() {
	KaichuResponseCapsule capsule = this.getNextResponseCapsuleNonBlocking();
        if(capsule == null) 
            return null;
        return capsule.response;
    }

    // The assumption is that the job IDs start at 0 and are incremented by one
    public KaichuResponse getNextResponseOrdered() throws InterruptedException {
        return this.getNextResponseCapsuleOrdered().response;
    }

    public KaichuResponse getNextResponseOrderedNonBlocking() {
	KaichuResponseCapsule capsule = this.getNextResponseCapsuleOrderedNonBlocking();
        if(capsule == null) 
            return null;
        return capsule.response;
    }

    // this is not right
    /*public synchronized int responsesPending() {
	return responseIDs.size() + jobManager.getPendingResponses() + ((DispatchQueue)jobManager.jobCreator).getQueueSize();
    }*/

    boolean offerResponse(KaichuResponseCapsule response) {

	synchronized(responsesSorted) {
	    if(KaichuManager.getDebug())
		System.out.println("SanchoManager: Acquired lock on responsesSorted...");
	    if(responseIDs.contains(response.getID())) {
		System.out.println(KaichuUtility.getDate() + " This response already returned! ");
		return false;
	    }
	    responseIDs.add(response.getID());
	    responsesSorted.offer(response);

	    // notify the outside thread in getNextResponseOrdered()
	    responsesSorted.notify();
	    if(KaichuManager.getDebug())
		System.out.println("SanchoManager: Releasing lock on responsesSorted...");
	}

	return true;
    }

    public void terminate() {
	//NanoHTTPD.stopServer();
	
	System.out.println(KaichuUtility.getDate() + " Sancho Manager terminating...");
	
	httpServer.destroy();
	for(JobStarter starterThread : jobStarters)
	    starterThread.shutdown();
	workerPool.poke.die = true;

	try {
	    registry.unbind("MasterStub");
	} catch(RemoteException ex) {
	    Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
	} catch(NotBoundException ex) {
	    Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
	} 

	try {
	    UnicastRemoteObject.unexportObject(master, true);
	} catch(NoSuchObjectException ex) {
	    Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	master.terminate();
	
	try {
	    for(JobStarter starterThread : jobStarters)
		starterThread.join();
	    httpServer.waitFor();
	} catch(InterruptedException ex) {
	    Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	System.out.println(KaichuUtility.getDate() + " Sancho Manager shut down.");
    }

    public long getRound() {
	return jobManager.getRound();
    }

    public void nextRound() {
	nextResponseID = 0;
	synchronized(responsesSorted) {
	    responseIDs.clear();
	}
	jobManager.nextRound();
    }

    public void start() {
	jobManager.enable();
    }
    
    public void stop() {
	jobManager.disable();
    }
    
    public ActiveWorkersListener activeWorkersListener = null;
    
    void activeWorkersNumChanged(int numActiveThreads) {
	if(activeWorkersListener != null)
	    activeWorkersListener.numWorkersChanged(numActiveThreads);
    }
    
    private void setUpMasterRMI() {
        if (System.getSecurityManager() == null)
            System.setSecurityManager(new RMISecurityManager());

        MasterStub stub = null;
        try {
            stub = (MasterStub)UnicastRemoteObject.exportObject(master, 0);
        } catch(RemoteException ex) {
            Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Couldn't create the Master stub");
            System.exit(-1);
        }

        // create a registry
        registry = null;
        try {
            registry = LocateRegistry.createRegistry(1099);
        } catch(RemoteException ex) {
            Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Could not start a registry");
            System.exit(-1);
        }

        String name = "MasterStub";
        try {
            registry.rebind(name, stub);
        } catch(RemoteException ex) {
            Logger.getLogger(KaichuManager.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Could not bind the stub to the registry");
            System.exit(-1);
        }

        System.out.println("Remote object set up!");
    }
    
    public int getAggregateSize() {
	return aggregateSize;
    }
    
    public void setAggregateSize(int size) {
	this.aggregateSize = size;
    }
    
    public void setAutomaticAggregation(boolean yorn) {
	this.automaticAggregationSizing = yorn;
    }
    
    public boolean doesAutomaticAggregation() {
	return this.automaticAggregationSizing;
    }

    class SanchoResponseCapsuleComparator implements Comparator<KaichuResponseCapsule> {

	@Override
	public int compare(KaichuResponseCapsule o1, KaichuResponseCapsule o2) {
	    if(o1.getID() == o2.getID()) {
		return 0;
	    } else if(o1.getID() < o2.getID()){
		return -1;
	    } else {
		return 1;
	    }
	}
    }
}
