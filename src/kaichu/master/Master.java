/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.rmi.RemoteException;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.ExecutorStub;
import kaichu.KaichuResponseCapsule;
import kaichu.KaichuTask;
import kaichu.KaichuUtility;
import kaichu.MasterStub;
import kaichu.ResourceBrokerStub;
import kaichu.ResourceSender;
import kaichu.RobustMessenger;
import kaichu.monitor.KaichuState;
import kaichu.monitor.StateHolder;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
class Master implements MasterStub, ResourceBrokerStub {
    
    WorkerPool workerPool;

  //  TreeMap<String, LinkedBlockingQueue<ExecutorStub>> resourceQueue = new TreeMap<String, LinkedBlockingQueue<ExecutorStub>>();

    boolean isLive = true;

    private final int maxConcurrentDownloadsFromMaster = 2;    // this number is not followed strictly because the 
    private Integer availableMasterDownloadSlots = maxConcurrentDownloadsFromMaster;
    private final Object sAccess = new Object();
   // Semaphore s = new Semaphore(n, false);
    private TreeMap<String, LinkedBlockingQueue<ResourceBrokerStub>> resourceQueues = new TreeMap<String, LinkedBlockingQueue<ResourceBrokerStub>>();
    private TreeMap<String, Boolean> resourceSentAtLeastOnce = new TreeMap<String, Boolean>();
    
    private final Object block = new Object();

    @Override
    public ResourceBrokerStub getR(String id, int workerID) throws RemoteException {
        workerPool.workerWaitingForDownloadToStart(id, workerID);
        while (true) {
            synchronized (sAccess) {
                //System.out.println("*************** " + workerID + " entering synchronized block");
		// in short, prioritise sending from workers
		if(!resourceSentAtLeastOnce.containsKey(id)) {
		    System.out.println("Error: Resource " + id + " hasn't been registered.");
		    System.exit(-1);
		}
		    
		if(!resourceSentAtLeastOnce.get(id)) {
		    workerPool.workerDownloadingResource(id, workerID);
		    resourceSentAtLeastOnce.put(id, true);
                    return this;
		}
		
		// send it from a worker if a worker has it
		LinkedBlockingQueue<ResourceBrokerStub> queue = resourceQueues.get(id);
		if(queue.size() != 0) {
		    workerPool.workerDownloadingResourceFromWorker(id, workerID);
		    return queue.poll();
		// if not, then send it from the master
		} else if(availableMasterDownloadSlots > 0) {
		    workerPool.workerDownloadingResource(id, workerID);
		    return this;
		}
		
            }
            
            // have to wait for free sources
            try {
                synchronized (block) {
                    block.wait();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Master.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    @Override
    public boolean getResourceDirectly(String id, ResourceBrokerStub recipient) throws RemoteException {

        synchronized (sAccess) {
            if (availableMasterDownloadSlots < 0) {
		if(KaichuManager.getDebug()) System.out.println("Resource " + id + " not available at the moment.");
                return false;   // sorry, try again
            }
            availableMasterDownloadSlots--;
        }

        Object resource = ResourceRegistry.getResourceToSend(id);
        boolean success;
        try {
            if (resource == null) {
                throw new Exception();
            }
            
            success = new RobustMessenger(workerPool.executorService).perform(recipient, new ResourceSender(recipient, id, resource));
	    if(KaichuManager.getDebug()) {
		if(!success) System.out.println("Error in Master.getResourceDirectly: RobustMessenger returned 'false'");
	    }
            
        } catch (Exception ex) {
	    if(KaichuManager.getDebug()) System.out.println("Exception in Master.getResourceDirectly(): " + ex.getMessage());
            success = false;
        } finally {    // always be able to refresh the semaphore
            synchronized (sAccess) {
                availableMasterDownloadSlots++;
            }
            synchronized (block) {
                block.notify();
            }
        }

        return success;
    }

    @Override
    public void sendResource(String id, Object resource) throws RemoteException {
	System.out.println("Master.sendResource not implemented!");
        throw new UnsupportedOperationException("This method should never be called on the Master!");
    }

    @Override
    public void iPossessResource(String id, ResourceBrokerStub worker) throws RemoteException {
        LinkedBlockingQueue<ResourceBrokerStub> queue = resourceQueues.get(id);

        if (queue == null) { // worker claims to have a resource that is not registered
            System.out.println("Warning: Worker claims to posses the resource '" + id + "' which is not registered at the Master.");
            return;
        }

        queue.add(worker);

        synchronized (block) {
            block.notify();
        }
    }
    
    @Override
    public KaichuTask getTask(Class taskClass, int workerID) throws RemoteException {
        if (!isLive)
	    return null;
	workerPool.workerDownloadingTask(taskClass, workerID);
        return MasterTaskRegistry.getTask(taskClass);
    }

    @Override
    public Object getResource(String id, int workerID) throws RemoteException {
        throw new UnsupportedOperationException("");
	/*if(!isLive)
	    return null;
	threadPool.threadWaitingForDownloadToStart(id, workerID);
	try {
	    downloadSemaphore.acquire();
	} catch(InterruptedException ex) {
	    Logger.getLogger(Master.class.getName()).log(Level.SEVERE, null, ex);
	}
	threadPool.threadDownloadingResource(id, workerID);
        return ResourceRegistry.getResource(id);*/

	// TODO: this is buggy in case where the worker dies before he gets to invoke gotTask()
    }

    @Override
    public void gotTask(int workerID) throws RemoteException {
	if(!isLive)
	    return;
	workerPool.workerCompletedDownloadingTask(workerID);
    }

    @Override
    public void gotResource(String id, int workerID) throws RemoteException {
	if(!isLive)
	    return;
	workerPool.workerCompletedDownloadingResource(id, workerID);
	//downloadSemaphore.release();
    }

    @Override
    public int hello(ExecutorStub stub, String host, int condorID) throws RemoteException {
        if(KaichuManager.getDebug()) System.out.println("Master.hello(): Worker at " + host + " saying hello!");
	if(!isLive)
	    return -1;
	stub.setPersist(true);
        return workerPool.addExecutor(stub, host, condorID);
    }

    public void terminate() {
	isLive = false;
        workerPool.shutdown();
	
    }

    @Override
    public boolean areWeStillOnWithThis(int workerID) throws RemoteException {
	if(!isLive)
	    return false;
        // This method will just throw an exception if the Master process is dead.
        // Otherwise it will just return.
	if(!workerPool.isActive(workerID)) {
	    if(KaichuManager.getDebug()) System.out.println(KaichuUtility.getDate() + " I have disowned worker " + workerID);
	    return false;
	}

	return true;
    }

    @Override
    public KaichuState getState() throws RemoteException {
	return StateHolder.state;
    }
    
    @Override
    public boolean isLive() throws RemoteException {
	return true;
    }

    @Override
    public void returnResponse(List<KaichuResponseCapsule> responses, int workerID) throws RemoteException {
	if(KaichuManager.getDebug())
	    System.out.println("Master: Worker " + workerID + " tries returning jobs from " + responses.get(0).getID());
	
	workerPool.returnResponse(responses, workerID);
    }

    @Override
    public void terminateWorker(int workerID) throws RemoteException {
	workerPool.terminateWorker(workerID);
    }

    void registerResource(String resourceName) {
	synchronized(sAccess) {
	    resourceQueues.put(resourceName, new LinkedBlockingQueue<ResourceBrokerStub>());
	    this.resourceSentAtLeastOnce.put(resourceName, false);
	}
    }

    @Override
    public String getIdentity() throws RemoteException {
        return "Master";
    }
    
}
