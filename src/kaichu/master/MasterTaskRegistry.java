/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import kaichu.KaichuTask;


/**
 *
 * @author Maciej Trybilo, Brunel University
 */
class MasterTaskRegistry extends TaskRegistry {

    protected MasterTaskRegistry() {
    }

    public static KaichuTask getTask(Class taskClass) {
	if(!map.containsKey(taskClass)) {
	    System.out.println("Worker requested a nonexistent task '" + taskClass.getCanonicalName() + "'!");
	}

	return map.get(taskClass).task;
    }

}
