/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuUtility;
import org.neilja.net.interruptiblermi.InterruptibleRMIThreadFactory;

/**
 *
 * @author Maciej Trybilo, Brunel University
 * 
 * Poke will aim to check on each worker every INTERROGATION_PERIOD, but will 
 * not make a check more often that every 50 milliseconds to avoid thrashing.
 */
class Poke extends Thread {
    
    final long INTERROGATION_PERIOD = 5 * 1000; 
    final long INTERROGATION_TIMEOUT = 2 * 1000;
    
    WorkerPool workerPool;
    volatile boolean die = false;
    
    private ConcurrentLinkedQueue<Worker> checking = new ConcurrentLinkedQueue();
    
    private Worker worker;
    int workerState = 1;
    
    ExecutorService executorService = Executors.newCachedThreadPool(InterruptibleRMIThreadFactory.getInstance());
    
    public Poke(WorkerPool workerPool) {
	this.workerPool = workerPool;
    }
    
    @Override
    public void run() {
	while(!die) {
	    if(!checking.isEmpty()) {

		worker = checking.poll();
		
		if(worker.state == Worker.State.DEAD) {
		    continue;
		}

		// Wait a small bit. This means we will wait at least 50 ms between each worker.
		// Thanks to that we will avoid thrashing when there are a lot of workers.
		try {
		    Thread.sleep(50);
		} catch(InterruptedException ex) {
		    Logger.getLogger(Poke.class.getName()).log(Level.SEVERE, null, ex);
		}

		// Wait until the interrogation period passes.
		long time = worker.getLastPoke() + INTERROGATION_PERIOD - System.currentTimeMillis();

		while(time > 0) {
		    //System.out.println("Poke: waiting for " + time + " to check worker " + worker.getID());
		    try {
			Thread.sleep(time);
		    } catch(InterruptedException ex) {
			Logger.getLogger(Poke.class.getName()).log(Level.SEVERE, null, ex);
		    }
		    time = worker.getLastPoke() + INTERROGATION_PERIOD - System.currentTimeMillis();
		}

		//System.out.println("Poke: checking worker " + worker.id);
		
		boolean success = true;
		int numTries = 2;
		
		do {
		    success = true;
		    Future messenger = executorService.submit(new Messenger());
		    try {
			messenger.get(INTERROGATION_TIMEOUT, TimeUnit.MILLISECONDS);
		    } catch(InterruptedException ex) {
			success = false;
			//Logger.getLogger(Poke.class.getName()).log(Level.SEVERE, null, ex);
		    } catch(ExecutionException ex) {
			success = false;
			//Logger.getLogger(Poke.class.getName()).log(Level.SEVERE, null, ex);
		    } catch(TimeoutException ex) {
			success = false;
			//Logger.getLogger(Poke.class.getName()).log(Level.SEVERE, null, ex);
		    }
		} while(--numTries > 0 && !success);
		
		if(!success)
		    workerState = 0;

		if(workerState != 0) {
		    //System.out.println("Poke: worker " + worker.id + " live!");
		    worker.setLastPoke(System.currentTimeMillis());
		    //worker.setLastPoke(workerState);
		    checking.offer(worker);
		} else { // timeout and not live
                    if(KaichuManager.getDebug())
                        System.out.println(KaichuUtility.getDate() + " Poke: Worker " + worker.id + ":" + worker.getCondorID() + " not responding!");
		    workerPool.dead(worker);
		    worker.setLastPoke(System.currentTimeMillis());
		    //worker.setLastPoke(workerState);
		}

		if(workerState == 1 && worker.state == Worker.State.WORKING) {
		    //System.out.println(KaichuUtility.getDate() + " WARNING: ****************** Worker " + worker.getID() + " idle while working! ******************");
		    //workerPool.dead(worker);
		}

	    } else {

		try {
		    Thread.sleep(100); // wait a bit if there are no workers at all
		} catch(InterruptedException ex) {
		    Logger.getLogger(Poke.class.getName()).log(Level.SEVERE, null, ex);
		}

	    }
	}

	if(KaichuManager.getDebug()) {
	    System.out.println("Poke exiting.");
	}
    }

    void addWorker(Worker worker) {
	checking.offer(worker);
    }
    
    private class Messenger implements Runnable {

	@Override
	public void run() {
	    workerState = worker.checkState();
	}
    }
}
