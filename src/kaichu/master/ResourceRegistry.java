/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.io.Serializable;
import kaichu.KaichuUtility;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */

public class ResourceRegistry extends AbstractResourceRegistry {

    static Master master;
    static boolean calculateSize = true;
   

    public static Object getResource(String id) {
        
        if (id == null) {
            return null;
        }

        if (map.containsKey(id)) {
            return map.get(id);
        } else { // fetch resource
            System.out.println("Don't have the resource " + id + ", fetching...");
            long start = System.currentTimeMillis();

            WorkerResourceRegistry.fetchResource(id); // if all went right the resource should be available locally
            
            System.out.println("Got it in " + (System.currentTimeMillis() - start) / 1000.0 + "s");
            return WorkerResourceRegistry.getResourceNoFetch(id);
        }
    }
    
    public static void registerResource(String resourceName, Object resource) {
	map.put(resourceName, resource);

        if(master != null) {  // check whether you are on the master or not
            master.registerResource(resourceName);

            System.out.println("Resource " + resourceName + " registered.");
            if(calculateSize) {
                long size = KaichuUtility.sizeOfSerialized((Serializable)resource);
                sizes.put(resourceName, size);
                System.out.println("Size of the resource: " + KaichuUtility.formatNumber(size));
            }
        }
    }
 
    
    static Object getResourceToSend(String id) {
	
	Object resource = map.get(id);
        
        if(resource == null)
            System.out.println("Worker requested nonexistent resource '" + id + "'!");
        return resource;
    }
    

}
