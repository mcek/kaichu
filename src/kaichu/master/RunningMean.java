/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class RunningMean {
    private int windowSize;
    private int minimumReasonable;
    
    private LinkedList<Double> values = new LinkedList<Double>();
    
    public RunningMean(int windowSize, int minimumReasonable) {
	this.windowSize = windowSize;
	this.minimumReasonable = minimumReasonable;
    }
    
    // puts a value and gets new average
    public Double putValue(double value) {
	values.offer(value);
	if(values.size() < minimumReasonable)
	    return null;
	
	while(values.size() > windowSize)
	    values.poll();
	
	// ** mean
	double result = 0.0;
        
        int count = 0;
        double previous = 0;
        double next = 0;
        
        Double[] vals = new Double[values.size()];
	vals = values.toArray(vals);
	Arrays.sort(vals);
        
        /*StringBuilder sb = new StringBuilder();
        for(Double val : vals) {
            sb.append(val);
            sb.append(" ");
        }
        System.out.println(sb.toString());*/
	
	for(Double val : vals) {
            count++;
            
            next = (result + val) / count;
            
            if((count > windowSize*(2./3)) && (val > 3*previous)) { // throw away outliers
                //System.out.println(val + " is an outlier!");
                break;
            }
            
	    result += val;
            previous = next;
        }
	
	return previous;
    }
    
    public void clear() {
	values.clear();
    } 
}
