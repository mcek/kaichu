/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.util.TreeMap;
import kaichu.ClassComparator;
import kaichu.KaichuTask;
import kaichu.KaichuTaskWrapper;
import kaichu.MasterStub;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class TaskRegistry {

    static MasterStub master;
    protected static TreeMap<Class, KaichuTaskWrapper> map = new TreeMap<Class, KaichuTaskWrapper>(new ClassComparator());

    
    protected TaskRegistry() {}
    

    public static void registerTask(KaichuTask task, KaichuManager manager) {
	
	KaichuTaskWrapper wrapper = new KaichuTaskWrapper(task, manager.getRunID());
	
	map.put(task.getClass(), wrapper);
	
	System.out.println("Task " + task.getClass().getCanonicalName() + " registered");
    }
    
    public static void clearTasks() {
	map.clear();
    }
}
