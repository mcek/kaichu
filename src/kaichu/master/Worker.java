/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.ExecutorStub;
import kaichu.KaichuJobCapsule;
import kaichu.monitor.StateHolder;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class Worker {
    List<KaichuJobCapsule> jobs = new ArrayList<KaichuJobCapsule>();
    private long pokeTime;
    int id;
    int condorId;
    String host;
    ExecutorStub executor;
    
    private static int nextID = 0;
       
    volatile State state = State.NULL;

    public enum State {NULL, INITIALISING, DL_TASK_M, DL_TASK_W, DL_RESOURCE_M, DL_RESOURCE_W, DL_JOB, WORKING, WAITING, DEAD}

    private long executionStartTime;

    boolean execute() throws RemoteException {
	this.executionStartTime = System.currentTimeMillis();
	return executor.execute(jobs);
    }
    
    public long getExecutionStartTime() {
	return executionStartTime;
    }

    int checkState() {
	try {
	    int result = executor.getState();
            if(result != 0)
                executor.updateCutoff(KaichuManager.cutoff);
            return result;
	} catch(RemoteException ex) {
	    if(KaichuManager.getDebug())
		Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
	    
	    return 0;
	}
    }
    
    public void setState(State state) {
	if(this.state != state)
	    StateHolder.setWorkerState(this.id, state);
	this.state = state;
    }
    
    public void setLastPoke(long time) {
	this.pokeTime = time;
	StateHolder.setLastPoke(time, id);
    }
   
    long getLastPoke() {
	return this.pokeTime;
    }

    int getCondorID() {
	return condorId;
    }

    void terminate() {
	try {
	    executor.terminate();
	} catch(RemoteException ex) {
	    if(KaichuManager.getDebug())
		Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
      
    boolean checkLive() {
	boolean live;
	try {
	    live = executor.isLive();
	} catch(RemoteException ex) {
	    if(KaichuManager.getDebug())
		Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
	    
	    return false;
	}
 
	return live;
    }
    
    Worker(String host, ExecutorStub executor, int condorId) {
	pokeTime = System.currentTimeMillis();
	this.id = Worker.getNextWorkerID();
	this.host = host;
	this.executor = executor;
	this.condorId = condorId;
    }
    
    private static synchronized int getNextWorkerID() {
	return nextID++;
    }
    
    public int getID() {
	return id;
    }
    
    public String getHost() {
	return host;
    }
    
    public void executeJob(List<KaichuJobCapsule> jobCapsules) {
	try {
	    executor.execute(jobCapsules);
	} catch(RemoteException ex) {
	    Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	this.setState(State.WORKING);
    }
}
