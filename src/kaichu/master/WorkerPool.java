/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.ExecutorStub;
import kaichu.KaichuJobCapsule;
import kaichu.KaichuResponseCapsule;
import kaichu.KaichuUtility;
import kaichu.monitor.StateHolder;
import org.neilja.net.interruptiblermi.InterruptibleRMIThreadFactory;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
class WorkerPool {
    
    private TreeMap<Integer, Worker> activeWorkers = new TreeMap<Integer, Worker>();
    
    LinkedBlockingQueue<Worker> idleWorkers = new LinkedBlockingQueue<Worker>();

    private TreeMap<Integer, Worker> deadWorkers = new TreeMap<Integer, Worker>();
    
    // this stores information about time it takes to download task/resource
    private TreeMap<Integer, Long> times = new TreeMap<Integer, Long>(); 

    
    JobManager jobManager;
    KaichuManager sanchoManager;
    Poke poke;

    ExecutorService executorService;

    WorkerPool(KaichuManager sanchoManager, JobManager jobManager) {
	this.sanchoManager = sanchoManager;
        this.jobManager = jobManager;
        //this.masterThread = masterThread;
	executorService = Executors.newCachedThreadPool(InterruptibleRMIThreadFactory.getInstance());
	poke = new Poke(this);
	poke.start();
    }

    public void workerDownloadingTask(Class taskClass, int workerID) {
	Worker worker = null;
	synchronized(this) {
	    worker = activeWorkers.get(workerID);
	}
	if(worker == null)
	    return;

	if(KaichuManager.getDebug()) 
	    System.out.println(KaichuUtility.getDate() + " Worker " + workerID + " is downloading task " + taskClass.getCanonicalName());
	worker.setState(Worker.State.DL_TASK_M);
	
	times.put(workerID, System.currentTimeMillis());
    }

    void workerWaitingForDownloadToStart(String id, int workerID) {
	Worker worker = null;
	synchronized(this) {
	    worker = activeWorkers.get(workerID);
	}
	if(worker == null)
	    return;

	worker.setState(Worker.State.WAITING);
    }

    public void workerDownloadingResource(String id, int workerID) {
	Worker worker = null;
	synchronized(this) {
	    worker = activeWorkers.get(workerID);
	}
	if(worker == null) 
	    return;

	if(KaichuManager.getDebug()) 
	    System.out.println(KaichuUtility.getDate() + " Worker " + workerID + " is downloading resource " + id);
	worker.setState(Worker.State.DL_RESOURCE_M);
	
	times.put(workerID, System.currentTimeMillis());
    }
    
    public void workerDownloadingResourceFromWorker(String id, int workerID) {
	Worker worker = null;
	synchronized(this) {
	    worker = activeWorkers.get(workerID);
	}
	
	if(worker == null) 
	    return;

	if(KaichuManager.getDebug()) 
	    System.out.println(KaichuUtility.getDate() + " Worker " + workerID + " is downloading resource " + id + " from another worker.");
	worker.setState(Worker.State.DL_RESOURCE_W);

	times.put(workerID, System.currentTimeMillis());
    }

    public void workerCompletedDownloadingTask(int workerID) {
	Worker worker = null;
	synchronized(this) {
	    worker = activeWorkers.get(workerID);
	}
	
	if(worker == null)
	    return;

	if(KaichuManager.getDebug())
	    System.out.println(KaichuUtility.getDate() + " Worker " + workerID + " completed downloading task in "
		+ (System.currentTimeMillis() - times.get(workerID))/1000.0 + "s");
	
	worker.setState(Worker.State.WORKING);
    }

    public void workerCompletedDownloadingResource(String id, int workerID) {
	Worker worker = null;
	synchronized(this) {
	    worker = activeWorkers.get(workerID);
	}
	
	if(worker == null)
	    return;

	double time = (System.currentTimeMillis() - times.get(workerID)) / 1000.0;

	if(ResourceRegistry.calculateSize) {
	    if(KaichuManager.getDebug()) System.out.println(KaichuUtility.getDate() + " Worker " + workerID + " completed downloading resource " + id + " in "
			+ time + "s (" + KaichuUtility.formatNumber((long)(ResourceRegistry.sizes.get(id)/time + 0.5)) + "/s)");
	} else { // don't know the size
	    if(KaichuManager.getDebug()) System.out.println(KaichuUtility.getDate() + " Worker " + workerID + " completed downloading resource " + id + " in "
			+ time + "s");    
	}

	worker.setState(Worker.State.WORKING);
    }

    public synchronized void dead(Worker worker) {
	if(worker.state == Worker.State.DEAD) // worker already declared dead
	    return;
	
	worker.setState(Worker.State.DEAD);
		
	deadWorkers.put(worker.id, activeWorkers.remove(worker.id));
	StateHolder.setActiveWorkers(activeWorkers.size());
	sanchoManager.activeWorkersNumChanged(activeWorkers.size());
	if(KaichuManager.getDebug()) 
	    System.out.println(KaichuUtility.getDate() + " " + activeWorkers.size() + " workers active");
	
	synchronized(worker) {
	    for(KaichuJobCapsule jobCapsule : worker.jobs)
		jobManager.iCannotDoTheJob(jobCapsule, worker);
	}
    }

/*    public synchronized void setLocalPool(ExecutorStub[] executors, int threadNum) {
        for(int i = 0; i < threadNum; i++) {
            CommunicatorThread thread = new CommunicatorThread(sanchoManager, jobManager, executors[i], this, "localhost", livenessMonitor, -1);
            addThread(thread);
            thread.setPriority(masterThread.getPriority() - 1);
            allThreads.add(thread);
            thread.start();
        }
    }*/

   public synchronized void shutdown() {
 
        activeWorkers.clear();
        deadWorkers.clear();
	
	poke.die = true;
	
	executorService.shutdownNow();
	
	try {
	    poke.join();
	} catch(InterruptedException ex) {
	    Logger.getLogger(WorkerPool.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    public synchronized int addExecutor(ExecutorStub executor, String host, int condorID) {
        Worker worker = new Worker(host, executor, condorID);
        System.out.println(KaichuUtility.getDate() + " Worker " + worker.id + ":" + condorID + " at " + host + " registered");
        addWorker(worker);
	worker.setState(Worker.State.INITIALISING);
	idleWorkers.offer(worker);
        return worker.id;
    }

    public synchronized boolean isActive(int threadID) {
	return activeWorkers.containsKey(threadID);
    }

    // note to self: don't change to synchronized
    private void addWorker(Worker worker) {
        activeWorkers.put(worker.id, worker);
	StateHolder.setActiveWorkers(activeWorkers.size());
	sanchoManager.activeWorkersNumChanged(activeWorkers.size());
	StateHolder.addWorker(worker);
	poke.addWorker(worker);
        System.out.println(KaichuUtility.getDate() + " " + activeWorkers.size() + " workers active");
    }

    void returnResponse(List<KaichuResponseCapsule> responses, int workerID) {
	
	Worker worker = null;
	synchronized(this) {
	    worker = activeWorkers.get(workerID);
	}
	
	if(KaichuManager.getDebug()) {
	    System.out.println("WorkerPool: Worker " + workerID + " returning responses from " + responses.get(0).getID());
	}

	if(worker == null) {  // this worker no longer here
	    if(KaichuManager.getDebug()) {
		System.out.println("Worker " + workerID + " no longer active!");
	    }
	    return;
	}

	synchronized(worker) {
	    if(KaichuManager.getDebug())
		System.out.println("WorkerPool: Got the lock on the worker " + worker.getID());

	    worker.setState(Worker.State.WAITING);
	    
	    jobManager.returnResponses(responses, worker);

	    idleWorkers.offer(worker);
	    if(KaichuManager.getDebug())
		System.out.println("WorkerPool: Worker " + workerID + " completed returning responses from " + responses.get(0).getID());
	}
    }

    void terminateWorker(int workerID) {
	Worker worker = activeWorkers.get(workerID);
	if(worker != null)
	    worker.terminate();
    }
}
