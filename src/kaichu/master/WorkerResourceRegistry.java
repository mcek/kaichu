/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.MasterStub;
import kaichu.ResourceBrokerStub;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class WorkerResourceRegistry {
    
    private static MasterStub master;
    private static int workerID;
    private static ResourceBrokerStub myResourceBrokerStub;
    
    public static void setMaster(MasterStub master) {
        WorkerResourceRegistry.master = master;
    }
    
    public static MasterStub getMaster() {
        return master;
    }
    
    public static void setWorkerID(int workerID) {
        WorkerResourceRegistry.workerID = workerID;
    }
    
    public static int getWorkerID() {
        return workerID;
    }
    
    public static void setWorkerMyResourceBrokerStub(ResourceBrokerStub myResourceBrokerStub) {
        WorkerResourceRegistry.myResourceBrokerStub = myResourceBrokerStub;
    }
    
    public static ResourceBrokerStub getWorkerMyResourceBrokerStub() {
        return myResourceBrokerStub;
    }
    
    static void fetchResource(String id) {
        boolean success = false;
        ResourceBrokerStub source = null;
	
        while(true) { 
	    
            try {

                source = master.getR(id, workerID);
                if (source == null) {
                    System.out.println("I asked for a non-existing resource. Panicking.");
                    System.exit(-1);
                }

                
	    } catch (RemoteException ex) {
                Logger.getLogger(WorkerResourceRegistry.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Ouuuupsss, cannot obtain the source for Resource " + id + " from the server. Panicking!");
		System.exit(-1);
            }

	    
	    try {
                System.out.println("Downloading resource from " + source.getIdentity() + "...");
		success = source.getResourceDirectly(id, myResourceBrokerStub);
		
                if (success) {
                    master.gotResource(id, workerID);   // confirm retrieval
                    master.iPossessResource(id, myResourceBrokerStub); // declare that have the resource
                    return;
                } else {
		    System.out.println("Error downloading the resource!");
		}
		
	    } catch(RemoteException ex) {
                //Logger.getLogger(ResourceRegistry.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Ouuuupsss, cannot retrieve Resource " + id + ". Will try again...");
		success = false;
	    }
	}
    }  
    
    public static Object getResourceNoFetch(String id) {
        if (id == null) {
            return null;
        }

        if(ResourceRegistry.map.containsKey(id))
            return ResourceRegistry.map.get(id);
        else {
	    return null;
        }
    }
}
