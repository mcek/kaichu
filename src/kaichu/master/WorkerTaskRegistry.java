/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.master;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuTask;
import kaichu.KaichuTaskWrapper;
import kaichu.MasterStub;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class WorkerTaskRegistry extends TaskRegistry {
    
    public static int workerID;

    public static KaichuTask getTask(Class taskClass, long runID) {

        //System.out.println("map.containsKey(taskClass) " + map.containsKey(taskClass));
        
	if(map.containsKey(taskClass)) {
            //System.out.println("map.get(taskClass) " + map.get(taskClass));
            //System.out.println("map.get(taskClass).getCurrentRunID() " + map.get(taskClass).getCurrentRunID());
	    if(map.get(taskClass).getCurrentRunID() == runID) {
		//System.out.println("I've got the task!");
		return map.get(taskClass).task;
	    } else {
                System.out.println("Got an old version of Task " + taskClass.getCanonicalName() + ", need to fetch new one");
                //System.out.println("The old runID is " + map.get(taskClass).getCurrentRunID() + ", the new one is " + runID);
		map.remove(taskClass);  // this doesn't really work regarding the class code
		//System.exit(-1);
	    }
	}
	// fetch task
	System.out.println("Don't have info on task " + taskClass.getCanonicalName() + ", fetching...");
	long start = System.currentTimeMillis();
	KaichuTask task = fetchTask(taskClass);
	map.put(taskClass, new KaichuTaskWrapper(task, runID));
	System.out.println("Got it in " + (System.currentTimeMillis() - start) / 1000.0 + "s");
	return task;
    }

    private static KaichuTask fetchTask(Class taskClass) {
	try {
	    KaichuTask task = master.getTask(taskClass, workerID);
	    master.gotTask(workerID);
	    return task;
	} catch(RemoteException ex) {
	    Logger.getLogger(TaskRegistry.class.getName()).log(Level.SEVERE, null, ex);
	    System.out.println("Ouuuupsss, cannot retrieve Task " + taskClass.getCanonicalName() + " from the server. Dieing!");
	    System.exit(-1);
	}
	return null;
    }
    
    public static void setMaster(MasterStub master) {
	WorkerTaskRegistry.master = master;
    }
}
