/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.monitor;

import java.text.SimpleDateFormat;
import java.util.Date;
import kaichu.master.Worker;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class StateHolder {


    private StateHolder() {}

    public static KaichuState state = new KaichuState();

    private static synchronized void incrementJobsDone() {
	state.jobsDone++;
    }

    public static void setActiveWorkers(int activeWorkers) {
	state.activeWorkers = activeWorkers;
    }

    public static synchronized void addWorker(Worker worker) {
	String[] row = new String[WorkersTableModel.colNum];

	row[0] = Integer.toString(worker.getID());
	row[1] = worker.getHost();
	row[2] = Integer.toString(0);
	row[3] = "NULL";
	row[4] = Long.toString(0);

	state.workersTableModel.rowData.put(worker.getID(), row);
    }

    public static synchronized void incrementJobsDoneByWorker(int workerID) {
	incrementJobsDone();
	state.workersTableModel.rowData.get(workerID)[2] = Integer.toString(Integer.parseInt(state.workersTableModel.rowData.get(workerID)[2]) + 1);
    }
    
    public static synchronized void setWorkerState(int workerID, Worker.State threadState) {
	if(threadState == Worker.State.WORKING)
	    state.workersTableModel.rowData.get(workerID)[3] = "Working";
	else if(threadState == Worker.State.WAITING)
	    state.workersTableModel.rowData.get(workerID)[3] = "Waiting";
	else if(threadState == Worker.State.DEAD)
	    state.workersTableModel.rowData.get(workerID)[3] = "Dead";	
	else if(threadState == Worker.State.INITIALISING) 
	    state.workersTableModel.rowData.get(workerID)[3] = "Init";
	else if(threadState == Worker.State.DL_RESOURCE_M)
	    state.workersTableModel.rowData.get(workerID)[3] = "DL Resource M";
	else if(threadState == Worker.State.DL_RESOURCE_W)
	    state.workersTableModel.rowData.get(workerID)[3] = "DL Resource W";
	else if(threadState == Worker.State.DL_TASK_M)
	    state.workersTableModel.rowData.get(workerID)[3] = "DL Task M";
	else if(threadState == Worker.State.DL_TASK_W)
	    state.workersTableModel.rowData.get(workerID)[3] = "DL Task W";  
	else if(threadState == Worker.State.DL_JOB)
	    state.workersTableModel.rowData.get(workerID)[3] = "DL Job"; 
    }

    public static synchronized void setJobsGenerated(int jobsGenerated) {
	state.jobsGenerated = jobsGenerated;
    }
    
    static SimpleDateFormat dateFormat = new SimpleDateFormat("EEE HH:mm:ss");
    
    public static synchronized void setLastPoke(long poke, int workerID) {
	Date date = new Date(poke);
	//System.out.println("Setting " + poke + " in worker " + workerID);
	state.workersTableModel.rowData.get(workerID)[4] = dateFormat.format(date);
	//System.out.println("Set " + state.workersTableModel.rowData.get(workerID)[4] + " in worker " + workerID);
    }
}
