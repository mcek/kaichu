/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.monitor;

import java.util.TreeMap;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class WorkersTableModel extends AbstractTableModel {


    final static String[] columnNames = {"ID", "Host", "Jobs Done", "State", "Last touch"};
    final static int colNum = columnNames.length;

    public WorkersTableModel() {
	rowData = new TreeMap<Integer, String[]>();
    }

    public TreeMap<Integer, String[]> rowData;

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public int getRowCount() { 
	return rowData.size();
    }

    @Override
    public int getColumnCount() { 
	return colNum;
    }

    @Override
    public Object getValueAt(int row, int col) {
	return rowData.get(row)[col];
    }

    @Override
    public boolean isCellEditable(int row, int col) {
	return false;
    }

    public void replaceData(TreeMap<Integer, String[]> rowData) {
	this.rowData = rowData;
	
	int firstRowInserted = -1;
	int lastRowInserted = -1;
	
	for(int row : rowData.keySet()) {
	    
	    if(!this.rowData.containsKey(row)) { // new row
		if(firstRowInserted == -1) 
		    firstRowInserted = row;
		lastRowInserted = row;
		
		this.rowData.put(row, rowData.get(row));
		continue;
	    }
	    
	    /*for(int col = 2; col < columnNames.length; col++) {
		if(!this.rowData.get(row)[col].equals(rowData.get(row)[col])) {
		    this.rowData.get(row)[col] = rowData.get(row)[col];
		    this.fireTableCellUpdated(row, col);
		}
	    }*/
	    
	    this.fireTableRowsUpdated(row, row);
	}
	
	if(firstRowInserted != -1)
	    //this.fireTableRowsInserted(firstRowInserted, lastRowInserted);
	    this.fireTableDataChanged(); // TODO this is lousy, but the previous one didn't want to work. will this work?
	
	//this.fireTableDataChanged();
    }


    
    /*public void setValueAt(Object value, int row, int col) {
        rowData[row][col] = value;
        fireTableCellUpdated(row, col);
    }*/
}