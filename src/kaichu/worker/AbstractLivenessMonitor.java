/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.worker;

import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuUtility;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
abstract class AbstractLivenessMonitor extends Thread {

    protected final static long MINUTE = 60000;
    protected final static long SECOND = 1000;

    protected long waitTimeInMs = 16 * SECOND;

    private long lastIdleTime;
    protected long timeoutInMs = 30 * SECOND;
    //boolean isExecuting;
    protected int countExecuting;

    protected boolean die = false;

    public synchronized void imExecuting() {
        countExecuting++;
    }

    public synchronized void imIdle() {
        countExecuting--;
        if(countExecuting == 0)
            lastIdleTime = System.currentTimeMillis();
    }
    
    public synchronized int getCountExecuting() {
	return countExecuting;
    }

    public long getLastIdleTime() { return lastIdleTime; }

    abstract public void discoveredNotExecuting();

    abstract public void discoveredNotExecutingAndTimeout();

    abstract public void discoverNotExecutingNotTimeout();

    @Override
    public void run() {
        System.out.println(KaichuUtility.getDate() + " Starting the Liveness Monitor");
        lastIdleTime = System.currentTimeMillis(); 

        while(!die) {

            try {
                //System.out.println("LM: Sleeping for " + waitTimeInMs);
                Thread.sleep(waitTimeInMs);
                //System.out.println("LM: Woke up!");
            } catch(InterruptedException ex) {
                Logger.getLogger(AbstractLivenessMonitor.class.getName()).log(Level.SEVERE, null, ex);
            }

            synchronized(this) {
                if(countExecuting == 0) {
                    discoveredNotExecuting();
                    if(System.currentTimeMillis() - lastIdleTime > timeoutInMs) {
                        discoveredNotExecutingAndTimeout();
                    } else {
                        discoverNotExecutingNotTimeout();
                    }
                }
            }
        }
    }
}
