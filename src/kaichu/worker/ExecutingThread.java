/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.worker;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuJobCapsule;
import kaichu.KaichuResponse;
import kaichu.KaichuResponseCapsule;
import kaichu.KaichuTask;
import kaichu.KaichuUtility;
import kaichu.master.WorkerTaskRegistry;


/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class ExecutingThread extends Thread {

    
    LivenessMonitor monitor;
    int numRequests = 0;
    long numJobs = 0;
    //boolean jobsAvailable = false;
    boolean die = false;
    
    LinkedBlockingQueue<List<KaichuJobCapsule>> jobsQueue = new LinkedBlockingQueue<List<KaichuJobCapsule>>();
    
    public ExecutingThread() {
    }
    
    public void setMonitor(LivenessMonitor monitor) {
	this.monitor = monitor;
    }
    
    @Override
    public void run() {
	while(!die) {
	    synchronized(this) {
		
		List<KaichuJobCapsule> jobCapsules = null;
		try {
		    jobCapsules = jobsQueue.take();
		} catch(InterruptedException ex) {
		    Logger.getLogger(ExecutingThread.class.getName()).log(Level.SEVERE, null, ex);
		    System.exit(-1);
		}

		if(die) {
		    return;
		}

		monitor.imExecuting();

		if(Main.debug) System.out.println("Executing..");

		List<KaichuResponseCapsule> responses = new ArrayList<KaichuResponseCapsule>();

		for(KaichuJobCapsule jobCapsule : jobCapsules) {
                    if(Main.debug) System.out.println("jobCapsule: " + jobCapsule);
                    if(Main.debug) System.out.println("job: " + jobCapsule.getJob());
                    KaichuTask task = WorkerTaskRegistry.getTask(jobCapsule.getJob().getTaskClass(), jobCapsule.getRunID());
                    KaichuResponse response = task.execute(jobCapsule.getJob().getParameters());
                    KaichuResponseCapsule responseCapsule = new KaichuResponseCapsule(response, jobCapsule);
		    responses.add(responseCapsule);
		    if(Main.debug) System.out.println("Done job " + responses.get(responses.size() - 1).getID());
		    numJobs++;
		}

		if(++numRequests % 1 == 0) {
		    System.out.println(KaichuUtility.getDate() + " " + numRequests + " requests, " + numJobs + " jobs handled...");
		}
		if(Main.debug) System.out.println("Returning responses...");
		try {
		    Main.master.returnResponse(responses, Main.myID);
		} catch(RemoteException ex) {
		    Logger.getLogger(ExecutingThread.class.getName()).log(Level.SEVERE, null, ex);
		    System.exit(-1);  // TODO: This is a bit dramatic. Try to recover better.
		}

		if(Main.debug) System.out.println("Done returning responses.");

		monitor.imIdle();
		//jobsAvailable = false;
	    }
	}
    }
}
