/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.worker;

import java.rmi.RemoteException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.ExecutorStub;
import kaichu.KaichuJobCapsule;
import kaichu.MasterStub;
import kaichu.ResourceBrokerStub;
import kaichu.ResourceSender;
import kaichu.RobustMessenger;
import kaichu.master.KaichuManager;
import kaichu.master.ResourceRegistry;
import kaichu.master.WorkerResourceRegistry;
import org.neilja.net.interruptiblermi.InterruptibleRMIThreadFactory;


/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class Executor implements ExecutorStub, ResourceBrokerStub {
    
    MasterStub master = null;

    int numRequests = 0;

    LivenessMonitor monitor;
    
    ExecutorService executorService;
    
    final ExecutingThread executingThread = new ExecutingThread();

    Executor(LivenessMonitor monitor, MasterStub master) {
        this.monitor = monitor;
        this.master = master;
        executorService = Executors.newCachedThreadPool(InterruptibleRMIThreadFactory.getInstance());
	executingThread.setMonitor(monitor);
	executingThread.start();
    }

    @Override
    public boolean execute(List<KaichuJobCapsule> jobCapsules) throws RemoteException {
	synchronized(Main.getInstance()) {
	    System.out.print("");
	} // block if the Main initialisation is not complete
	
	if(Main.debug) System.out.println("Before the synchronized block in execute()");
	
	if(executingThread.jobsQueue.remainingCapacity() == 0)
	    return false;
	
	try {
	    executingThread.jobsQueue.put(jobCapsules);
	} catch(InterruptedException ex) {
	    Logger.getLogger(Executor.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	if(Main.debug) System.out.println("Returning from execute()");
	return true;
    }
    
    

    /*@Override
    public List<SanchoResponseCapsule> execute(List<SanchoJob> jobs) throws RemoteException {
	SanchoResponseCapsule response;

	synchronized(Main.getInstance()) {
	    System.out.print("");
	} // block if the Main initialisation is not complete

	monitor.imExecuting();

	System.out.println("Executing..");
	
	List<SanchoResponseCapsule> responses = new ArrayList<SanchoResponseCapsule>();

	for(SanchoJob job : jobs)
	    responses.add(WorkerTaskRegistry.getTask(job.getTaskClass(), job.getRunID()).execute(job));

	if(++numRequests % 1 == 0) {
	    System.out.println(SanchoUtility.getDate() + " " + numRequests + " requests handled...");
	}

	monitor.imIdle();

	return responses;
    }*/

/*    public void sendTask(String id, SanchoTask task) throws RemoteException {
        Tasks.map.put(id, task);
    }

    public void sendResource(String id, Object resource) throws RemoteException {
        Resources.map.put(id, resource);
    }

*/

    @Override
    public void terminate() throws RemoteException {
        System.out.println("Sent terminate signal, bye!");
        System.exit(0);
    }

    @Override
    public void setPersist(boolean persist) throws RemoteException {
	Main.persist = persist;
    }

    @Override
    public boolean isLive() throws RemoteException {
	return true;
    }
    
    boolean isBusyLock = false;
    
    
    // This method is used in P2P resource dissemination
    @Override
    public boolean getResourceDirectly(String id, ResourceBrokerStub worker) throws RemoteException {

        // refuse to send anything if currently sending
        // will happen only rarely
        synchronized (this) {
            if (isBusyLock) {
                System.out.println("Warning: I was asked to send a resource while I'm busy.");
                return false;
            }
            isBusyLock = true;
        }

        Object resource = WorkerResourceRegistry.getResourceNoFetch(id);

        boolean success;

        if (resource == null) { // I don't have the resource, this should never happen
            System.out.println("Warning: I was asked to send the resource '" + id + "' which I don't possess.");
            success = false;
        } else {
            success = new RobustMessenger(executorService).perform(worker, new ResourceSender(worker, id, resource));
        }

        isBusyLock = false; // always release the lock and do it before you report back to the master

        if (resource != null) { // report readiness to send this resource to further workers
            master.iPossessResource(id, this);
        }

        return success;
    }

    @Override
    public void sendResource(String id, Object resource) throws RemoteException {
	if(KaichuManager.getDebug()) System.out.println("In Executor.sendResource " + id + " " + resource);
        ResourceRegistry.registerResource(id, resource);
    }

    @Override
    public void updateCutoff(double cutoff) throws RemoteException {
	KaichuManager.cutoff = cutoff;
    }

    @Override
    public int getState() throws RemoteException {
	return monitor.getCountExecuting() + 1; // 1 if idle, greater than one if executing
    }

    @Override
    public String getIdentity() throws RemoteException {
        return "Worker";
    }
}