/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.worker;

import java.rmi.RemoteException;
import kaichu.KaichuUtility;
import kaichu.MasterStub;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class LivenessMonitor extends AbstractLivenessMonitor {

    MasterStub master;
    int id = -1;

    public LivenessMonitor(MasterStub master) {
        this.master = master;
    }

    @Override
    public void discoveredNotExecuting() {
//        System.out.println(SanchoUtility.getDate() + " LM: Haven't been executing for " + (System.currentTimeMillis() - getLastIdleTime()) + "ms!");
    }

    @Override
    public void discoveredNotExecutingAndTimeout() {
	boolean disowned = false;
	boolean exceptionRaised;

        System.out.println(KaichuUtility.getDate() + " LM: I've waited for " + (System.currentTimeMillis() - getLastIdleTime()) + "ms for job.");
        System.out.println(KaichuUtility.getDate() + " LM: Trying to ask the master if we're still at it...");
	
        try {
            disowned = !master.areWeStillOnWithThis(id);
	    exceptionRaised = false;
        } catch(RemoteException ex) {
	    exceptionRaised = true;
	}

	if(exceptionRaised)
	    System.out.println(KaichuUtility.getDate() + " LM: Master is gone..");
	else if(disowned)
	    System.out.println(KaichuUtility.getDate() + " LM: Master is live, but has disowned me!");


	if(exceptionRaised || disowned) {
	    synchronized(Main.getInstance()) {
		Main.getInstance().notify();
	    }
	    die = true;
	    return;
        }
	
        System.out.println(KaichuUtility.getDate() + " LM: Ok, master still live.");
    }

    @Override
    public void discoverNotExecutingNotTimeout() {
  //      System.out.println(SanchoUtility.getDate() + " LM: Will wait some more.");
    }
}
