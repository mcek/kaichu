/*
 * Copyright 2012 Maciej Trybiło, Brunel University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kaichu.worker;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.ExecutorStub;
import kaichu.KaichuUtility;
import kaichu.MasterStub;
import kaichu.ResourceBrokerStub;
import kaichu.master.ResourceRegistry;
import kaichu.master.WorkerResourceRegistry;
import kaichu.master.WorkerTaskRegistry;
import org.neilja.net.interruptiblermi.InterruptibleRMISocketFactory;

/**
 *
 * @author Maciej Trybilo, Brunel University
 */
public class Main {

    /**
     * @param args the command line arguments
     */

    static LivenessMonitor monitor;
    static MasterStub master = null;

    static String masterHost;
    static String myHost = null;

    static boolean persist = true; // whether persist when the master is dead

    static int myID;
    static int condorID;

    static Executor executor;

    static Main me;
    
    static boolean debug = false;
    
    public static void main(String args[]) {
        
        if(args.length < 2) {
            System.out.println("Usage: Kaichu <masterHost>");
            System.exit(-1);
        }
	
	masterHost = args[1];
	
	System.out.println("Master host: " + masterHost);
	
	/*if(args.length > 2) {
	    myHost = args[2];
	    System.out.println("My host as chosen by the user: " + myHost);
	}*/
	if(args.length > 2) {
	    System.out.println("Debug!!");
	    if(args[2].equals("-debug"))
		Main.debug = true;
	}
	
	if(Main.debug)
	    System.out.println("Debug mode.");
	
	new Main();
    }
    
    //SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
    //SetThreadExecutionState(ES_SYSTEM_REQUIRED);
/*public interface Kernel32 extends StdCallLibrary { 
    // Method declarations, constant and structure definitions go here
    final int ES_SYSTEM_REQUIRED = 0x00000001;
    int SetThreadExecutionState(int esFlags);
Kernel32 INSTANCE = (Kernel32)
    Native.loadLibrary("kernel32", Kernel32.class);
// Optional: wraps every call to the native library in a
// synchronized block, limiting native calls to one at a time
Kernel32 SYNC_INSTANCE = (Kernel32)
    Native.synchronizedLibrary(INSTANCE);
}



Kernel32 lib = Kernel32.INSTANCE;

*/
    public Main() {
	
	//lib.SetThreadExecutionState(Kernel32.ES_SYSTEM_REQUIRED);

	me = this;

	condorID = KaichuUtility.getCondorID();

	System.out.println("My Condor ID is " + condorID);
	
	Random r = new Random();

	do {

	    synchronized(this) {  // prevent execution before all of the set up is done

		while(!setUpSlaveRMI(masterHost) && persist) {
		    try {
			// wait for 25-30 seconds
			int waiting = 25 + r.nextInt(6);
			System.out.println(KaichuUtility.getDate() + " Waiting for " + waiting + " seconds...");
			this.wait(waiting * 1000);
		    } catch(InterruptedException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(-1);
		    }
		}

                WorkerResourceRegistry.setWorkerID(myID);
		monitor.id = WorkerTaskRegistry.workerID = myID;
                
		monitor.start();

	    }

	    System.out.println("Ready!");

	    try {
		synchronized(this) {
		    this.wait(); // wait until the liveness monitor realises that we lost connection with the master
		}
		monitor.join();

	    } catch(InterruptedException ex) {
		Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		System.exit(-1);
	    }

	    // I've been notified, time to reset or die
	} while(persist);
    }

    static public Main getInstance() {
	return me;
    }

    private boolean setUpSlaveRMI(String registryHost) {
	
	//SanchoTaskRegistry.clearTasks();
	ResourceRegistry.clearResources();

        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        //executor = new Executor();

        if(registryHost == null) {
            System.out.println(KaichuUtility.getDate() + " Please supply the rmi master address");
            System.exit(-1);
        }

        Registry registry = null;
        try {
            System.out.println(KaichuUtility.getDate() + " Locating the registry at " + registryHost + "...");
            registry = LocateRegistry.getRegistry(registryHost);
        } catch(RemoteException ex) {
	    System.out.println("Could not locate the registry.");
	    return false;
        }

	master = null;

	System.out.println(KaichuUtility.getDate() + " Locating the master object...");

        try {
            master = (MasterStub)registry.lookup("MasterStub");
        } catch(RemoteException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
	    System.out.println(KaichuUtility.getDate() + " RemoteException raised while trying to connect to master");
	    return false;
        } catch(NotBoundException ex) {
            //Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
	    System.out.println(KaichuUtility.getDate() + " NotBoundException raised while trying to connect to master");
	    return false;
        }
        
        if(Main.debug) System.out.println("Master: " + Main.master);

	WorkerTaskRegistry.setMaster(Main.master);
	WorkerResourceRegistry.setMaster(Main.master);

	monitor = new LivenessMonitor(master);

        // register the executor
        executor = new Executor(monitor, master);
        
        ExecutorStub stub = null;

	final InterruptibleRMISocketFactory socketFactory = new InterruptibleRMISocketFactory();

        try {
            stub = (ExecutorStub)UnicastRemoteObject.exportObject(executor, 0, socketFactory, socketFactory);
        } catch(RemoteException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(KaichuUtility.getDate() + " Couldn't create the Executor stub");
            return false;
        }
	
	WorkerResourceRegistry.setWorkerMyResourceBrokerStub((ResourceBrokerStub)stub);
        
	InetAddress localHost = null;
	try {
		localHost = Main.getIPv4InetAddress();
	} catch(SocketException ex) {
	    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
	} catch(UnknownHostException ex) {
		Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
	}

	String localHostAddress = localHost.getHostAddress();
        
	System.out.println(KaichuUtility.getDate() + " My address is: " + localHostAddress);
	if(localHost.isLoopbackAddress()) {
	    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!  Warning! The local host is returned as a loopback address.");
	}
	
        try {
            System.out.println(KaichuUtility.getDate() + " Saying hello to the master...");
            myID = master.hello(stub, localHostAddress, condorID);
        } catch(RemoteException e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } 

        System.out.println(KaichuUtility.getDate() + " Worker RMI set up! My ID is " + myID);
	return true;
    }

    private void waitForever() {
	while(true) {
	    synchronized(this) {
		try {
		    this.wait();
		} catch(InterruptedException ex) {
		    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	}
    }

    static private InetAddress getIPv4InetAddress() throws SocketException, UnknownHostException {

	String os = System.getProperty("os.name").toLowerCase();

	if(os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0) {
	    NetworkInterface ni = NetworkInterface.getByName("eth0");

	    if(ni == null) {
		return null;
	    }

	    Enumeration<InetAddress> ias = ni.getInetAddresses();

	    InetAddress iaddress;

	    do {
		iaddress = ias.nextElement();
	    } while(!(iaddress instanceof Inet4Address));

	    return iaddress;
	}

	return InetAddress.getLocalHost();  // for Windows or OS X it should work well
    }


    /*private static InetAddress getFirstNonLoopbackAddress(boolean preferIpv4, boolean preferIPv6) throws SocketException {
	Enumeration en = NetworkInterface.getNetworkInterfaces();
	while(en.hasMoreElements()) {
	    NetworkInterface i = (NetworkInterface)en.nextElement();
	    for(Enumeration en2 = i.getInetAddresses(); en2.hasMoreElements();) {
		InetAddress addr = (InetAddress)en2.nextElement();
		if(!addr.isLoopbackAddress()) {
		    if(addr instanceof Inet4Address) {
			if(preferIPv6) {
			    continue;
			}
			return addr;
		    }
		    if(addr instanceof Inet6Address) {
			if(preferIpv4) {
			    continue;
			}
			return addr;
		    }
		}
	    }
	}
	return null;
    }*/

}